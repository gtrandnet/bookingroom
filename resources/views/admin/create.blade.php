@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('create') }}
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color: #2C3E50;color:white">BookingRooom</div>

                <div class="card-body">
                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif

                    <form method="post" action="{{route('room.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="title">Название Комнаты</label>
                            <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp" placeholder="Название Комнаты" required>  
                            <input class="mt-3" type="file" id="image" name="image">                 
                        </div>
                      <input type="submit" class="btn btn-primary" value="Добавить комнату">
                        
                    </form>
                    @error('title')
                        <div class="alert alert-warning mt-3" role="alert">
                            Такая комната уже существует!!
                        </div>
                    @enderror
                    @error('image')
                        <div class="alert alert-warning mt-3" role="alert">
                            Ошибка в поле картинка!!
                        </div>
                    @enderror
                </div>
            </div>
        </div>
    </div>
</div>
@endsection