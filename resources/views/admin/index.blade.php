@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">BookingRooom</div>
                
                <div class="card-body">
                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif

                    @foreach($data as $row)
                       <div class="form-group">
                            <a  class="btn btn-primary mt-4" href="{{route('room.show', $row->id)}}">{{$row->title}}</a>                  
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection