@extends('layouts.app')

@section('content')
  {{ Breadcrumbs::render('admin') }}
<div class="container-fluid">
@if (session('message'))
    <div class="alert alert-success" role="alert">
        {{ session('message') }}
    </div>
@endif
    <table class="table" id="datatable">
  <thead class="thead" style="background-color: #2C3E50;color:white">
    <tr>
      <th scope="col">N:</th>
      <th scope="col">Название</th>
      <th scope="col">Удалить</th>
      <th scope="col">Изменить</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $row)
    <tr>
      <th scope="row">{{$row->id}}</th>
      <td>{{$row->title}}</td>
      <td>
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalDelete{{$row->id}}">
          Удалить
        </button>
      </td>
      <td><a class="btn btn-warning" href="{{route('room.edit', $row->id)}}">Изменить</a></td>
    </tr>
    <!-- Modal -->
    <div class="modal fade" id="modalDelete{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Безвозвратное удаление комнаты!!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Подтвердите действие
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Отменить</button>
            <form method="post" action="{{route('room.destroy', $row->id)}}">
              @csrf
              @method('DELETE')
              <button class="btn btn-danger">Продолжить</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </tbody>
</table>
</div>
@endsection
<!-- Button trigger modal -->


