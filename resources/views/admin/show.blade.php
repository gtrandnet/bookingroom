@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">BookingRooom</div>

                <div class="card-body">
                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
                       <div class="form-group">
                            <h3>{{$data->title}}</h3>
                            <a class="btn btn-warning" href="{{route('room.edit', $data->id)}}">Изменить</a>
                            <form method="post" action="{{route('room.destroy', $data->id)}}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger mt-4">Удалить</button>
                            </form>                  
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection