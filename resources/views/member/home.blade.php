@extends('layouts.app')

@section('content')
<style type="text/css">
  #p{
    
  }
</style>
<?php $i = true; ?>
<div class="container-fluid">
    @if (session('success'))
      <div class="alert alert-success" role="alert">
        {{ session('success') }}
      </div>
    @endif
    @if(isset($data))
      <div class="row pt-5 mb-5"> 
        @foreach($data as $row)      
          <div class="col-12 col-md-4 col-xl-4 pr-xl-3 pt-md-3">
            <div class="card rounded border-0 shadow-lg  mb-5">
              <div class="card-body p-4">
                <h3 class="card-title"><i class="far fa-chart-bar mr-2 mr-lg-3 text-primary fa-lg fa-fw"></i><a href="{{route('rooms.show', $row->id)}}"></a></h3>
                <div>
                  <img style="height:1%;width:100%;" src="images/{{$row->image}}">
                </div>
                <p class="card-text" style="font-size: 2rem">{{$row->title}}</p>
                <a class="btn btn-primary" href="{{route('rooms.show', $row->id)}}">Подробнее</a>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter{{$row->id}}">
                  Забронировать
                </button>
                @if($row->bookingprocess->where('status','!=', 0)->count() != 0)
                  @foreach($row->bookingprocess->where('status','!=', 0) as $key)
                    @if(\App\Services\Helper::isToday($key->booking_start))
                      @if(\App\Services\Helper::isNow($key->booking_start, $key->booking_finish))
                        <p class="bg-warning" id="p" style="
                    width:80%;
                    margin-top: 5%;
                    padding: 2%;
                    border-radius: 5px;
                    height: 50px;
                    text-align: center;
                  ">Занято</p>
                      @endif
                      <p class="bg-danger" id="p" style="
                    width:80%;
                    margin-top: 5%;
                    padding: 2%;
                    border-radius: 5px;
                    height: 50px;
                    text-align: center;
                  ">Сегодня занято с {{date('h:i',strtotime($key->booking_start))}} до {{date('h:i',strtotime($key->booking_finish))}}</p>
                      <?php $i = false; ?>
                      @break
                    @endif
                  @endforeach
                @else
                  <p class="bg-success" id="p" style="
                    width:80%;
                    margin-top: 5%;
                    padding: 2%;
                    border-radius: 5px;
                    height: 50px;
                    text-align: center;
                  ">Сегодня свободно</p>
                  <?php $i = false; ?>
                @endif
                @if($i)
                  <p class="bg-success" id="p" style="
                    width:80%;
                    margin-top: 5%;
                    padding: 2%;
                    border-radius: 5px;
                    height: 50px;
                    text-align: center;
                  ">Сегодня свободно</p>
                @endif
              </div>
            </div>
          </div>
          @include('member.modal') 
        @endforeach
      </div>
    @endif 
</div>

<script>
    $(document).ready(function() {
    var max_fields_limit      = 8; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $('.add_more_button').click(function(e){ //click event on add more fields button having class add_more_button
        e.preventDefault();
        if(x < max_fields_limit){ //check conditions
            x++; //counter increment
            $('.input_fields_container_part').append('<div class="mt-4"><input class="form-control" style="width:50%; float:left" type="text" name="name[]" required autofocus /><a href="#" class="remove_field btn btn-danger ml-2">Remove</a></div>'); //add input field
        }
    });  
    $('.input_fields_container_part').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
@endsection