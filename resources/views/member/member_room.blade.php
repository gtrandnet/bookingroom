@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('rooms') }}
<div class="container-fluid">
                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
                        <table class="table" id="datatable">
                                    <thead class="thead" style="background-color: #2C3E50;color:white">
                                      <tr>
                                        <th scope="col">N:</th>
                                        <th scope="col">Комната</th>
                                        <th scope="col">Расписание на:</th>
                                        <th scope="col">От</th>
                                        <th scope="col">До</th>
                                        <th scope="col">Продлить</th>
                                        <th scope="col">Завершить</th>
                                      </tr>
                                    </thead>
                          <tbody>
                        @foreach($data as $row)
                          @if($row->status != 0){{-- @if(strtotime($row->booking_start) < strtotime(\Carbon\Carbon::tomorrow()) ) --}}
                            @if(strtotime($row->booking_start) < strtotime(\Carbon\Carbon::now()->addDays(2)) )
                              <tr>
                                <th scope="row">{{$row->id}}</th>
                                <th scope="row"><a href="{{route('rooms.show', $row->room_id)}}">{{$row->room_title}}</a></th>
                                <th scope="row">Сегодня</th>
                                <th scope="row">{{date('h:i',strtotime($row->booking_start))}}</th>
                                <th scope="row">{{date('h:i',strtotime($row->booking_finish))}}</th>
                                <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter{{$row->id}}">
                                        Продлить
                                      </button>
                                </td>
                                <th scope="row">
                                  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalDelete{{$row->id}}">
                                    Завершить
                                  </button>
                                  
                                </th>
                              </tr>
                              <!-- Modal -->
                              <div class="modal fade" id="modalDelete{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Безвозвратное завершение комнаты!!</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      Подтвердите действие
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-success" data-dismiss="modal">Отменить</button>
                                      <form method="post" action="{{route('booking.destroy', $row->id)}}">
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Завершить</button>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              @include('member.secmodal') 
                            @endif
                          @endif
                        @endforeach
                        </tbody>
                      </table>
                       
</div>
@endsection

