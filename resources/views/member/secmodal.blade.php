<!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Продление комнаты {{$row->title}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{route('booking.extension')}}" method="post">
                                          @csrf
                                            <div class="row">
                                                <div class="col-xs-12">
                                                  <h4 class="m-3">Продлить до</h4>
                                                  <input type="text" class="form-control m-3" id="datetimepicker2{{$row->id}}" name="finish" required>   
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                $(function () {
                                                  
                                                  $('#datetimepicker2{{$row->id}}').datetimepicker({
                                                    locale: 'ru',
                                                    format: 'Y-MM-D HH:mm:ss',  
                                                  });
                                                });
                                              </script>
                                              <!------ Include the above in your HEAD tag ---------->
                                            <div class="modal-footer">
                                                <input type="hidden" name="start" value="{{$row->booking_start}}">
                                                <input type="hidden" name="id" value="{{$row->id}}">
                                                <input type="hidden" name="room_id" value="{{$row->room_id}}">
                                                <input type="hidden" name="room_title" value="{{$row->room_title}}">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                                <button type="submit" class="btn btn-primary">Сохранить</button>
                                            </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>    