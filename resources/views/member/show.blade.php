@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('more', $data[0]->room_id) }}
<div class="container-fluid">
    <table class="table" id="datatable">
  <thead class="thead" style="background-color: #2C3E50;color:white">
    <tr>
      <th scope="col">N:</th>
      <th scope="col">Комната</th>
      <th scope="col">Расписание на :</th>
      <th scope="col">Занято От</th>
      <th scope="col">Занято До</th>
      <th scope="col">Участники</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $row)
    <tr>
      @if($row->status != 0){{-- @if(strtotime($row->booking_start) < strtotime(\Carbon\Carbon::tomorrow()) ) --}}
        @if(strtotime($row->booking_start) < strtotime(\Carbon\Carbon::now()->addDays(2)) )
            <th scope="row">{{$row->id}}</th>
            <th scope="row">{{$row->room_title}}</th>
            <th scope="row">Сегодня</th>
            <td>{{date('h:i',strtotime($row->booking_start))}}</td>
            <td>{{date('h:i',strtotime($row->booking_finish))}}</td>
            <td><a class="btn btn-primary" href="{{route('booking.options', $row->id)}}">Показать</a></td>
        @endif
      @endif
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection