<!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Забронирование комнаты {{$row->title}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{route('booking')}}" method="post">
                                          @csrf
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                  <h4>Забронирование от</h4>
                                                  <input type="text" class="form-control" id="datetimepicker1{{$row->id}}" name="start" required>   
                                                </div>
                                                <div class="col-md-12 col-xs-12">
                                                  <h4>Забронирование до</h4>
                                                  <input type="text" class="form-control" id="datetimepicker2{{$row->id}}" name="finish" required>   
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                $(function () {
                                                  
                                                  $('#datetimepicker2{{$row->id}}').datetimepicker({
                                                    locale: 'ru',
                                                    format: 'Y-MM-D HH:mm:ss',  
                                                  });
                                                  $('#datetimepicker1{{$row->id}}').datetimepicker({
                                                    locale: 'ru',
                                                    format: 'Y-MM-D HH:mm:ss', 
                                                  });
                                                });
                                              </script>
                                              <!------ Include the above in your HEAD tag ---------->

                                              <div class="input_fields_container_part">
                                                <div class="mt-5 mb-6">
                                                     <button class="btn btn-sm btn-success add_more_button ml-2 mb-3" style="font-size: 1rem;"><i class="fa fa-plus-square" aria-hidden="true"></i> Добавить участника</button>
                                                </div>
                                              </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="rooms_id" value="{{$row->id}}">
                                                <input type="hidden" name="room_title" value="{{$row->title}}">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                                <button type="submit" class="btn btn-primary">Сохранить</button>
                                            </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>