@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('partner', $room_id[0]->room_id) }}
<div class="container-fluid">
    @if($partners->count() == 0)
        <h4><span>В этом совещании отсутсвуют участники!!</span></h4>
    @elseif(!empty($partners))
        <h4><span>В этом совещании участвуют</span></h4>
               <table class="table" id="datatable">
                  <thead class="thead" style="background-color: #2C3E50;color:white">
                    <tr>
                      <th scope="col">N:</th>
                      <th scope="col">Имя</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($partners as $partner)
                        <tr>
                          <th scope="row">{{$partner->id}}</th>
                          <th scope="row">{{$partner->name}}</th>
                        </tr>
                    @endforeach 
                  </tbody>
                </table>
        </section>
    @endif
</div>
@endsection