<?php
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CreateAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin = User::create([
            'name' => "superadmin",
            'email' =>"superadmin@alif.tj",
            'type' =>'super_admin',
            'password' => Hash::make('12345678'),
        ]);
        $admin = User::create([
        	'name' => "admin",
        	'email' =>"admin@alif.tj",
        	'type' =>'admin',
        	'password' => Hash::make('12345678'),
        ]);
        $member = User::create([
        	'name' => "member",
        	'email' =>"member@alif.tj",
        	'type' =>'member',
        	'password' => Hash::make('12345678'),
        ]);

    }
}
