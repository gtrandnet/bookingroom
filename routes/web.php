<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'MemberController@index')->name('member');


Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{

	Route::match(['get', 'post'], '/admin', 'HomeController@admin');
	Route::resource('room', 'RoomsController');

});


Route::get('/member', 'MemberController@index')->name('member');
Route::get('/rooms/{id}', 'MemberController@show')->name('rooms.show');
Route::post('/booking', 'MemberController@insert')->name('booking');
Route::get('/options/{id}', 'MemberController@getOptions')->name('booking.options');
Route::get('/member/rooms/{id}', 'MemberController@bookingRooms')->name('booking.rooms');
Route::post('/extension', 'MemberController@extension')->name('booking.extension');
Route::post('/bookingdestroy/{id}', 'MemberController@bookingDestroy')->name('booking.destroy');



