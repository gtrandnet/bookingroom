<?php

Breadcrumbs::register('home', function ($breadcrumbs) {

$breadcrumbs->push('Главная', url('/home'));

});
Breadcrumbs::for('admin', function ($breadcrumbs) {

$breadcrumbs->parent('home');

$breadcrumbs->push('Панель администратора', url('/admin'));

});
Breadcrumbs::for('more', function ($breadcrumbs, $id) {

$breadcrumbs->parent('home');

//$breadcrumbs->push('Подробнее', url('/rooms/{id}'));
$breadcrumbs->push('Подробнее', url("/rooms/$id"));

});


Breadcrumbs::for('edit', function ($breadcrumbs) {

$breadcrumbs->parent('admin');

$breadcrumbs->push('Изменить', url('/room/{id}/edit'));

});


Breadcrumbs::for('rooms', function ($breadcrumbs) {

$breadcrumbs->parent('home');

$breadcrumbs->push('Мои забронированные комнаты', url('/admin'));

});


Breadcrumbs::for('partner', function ($breadcrumbs, $id) {

$breadcrumbs->parent('more', $id);

$breadcrumbs->push('Участники', url('/options/{id}'));

});

Breadcrumbs::for('create', function ($breadcrumbs) {

$breadcrumbs->parent('admin');
$breadcrumbs->push('Создать комнату', url('/room/create'));

});
