<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * 
 */
class HelperService extends Facade
{
	
	protected static function getFacadeAccessor() {
	 	return 'helper';
	}
}