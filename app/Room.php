<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{

	protected $fillable = [
     'title', 'status'
    ];

    public function bookingprocess(){
    	return $this->hasMany(BookingProcess::class);
    }
}
