<?php

namespace App\Http\Controllers;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use App\Room;
use App\Services\Helper;
use HelperService;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Room::all();
        return view('admin.home', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        request()->validate([
            'title' => 'required|unique:rooms|string|max:200',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $image = request()->image;
        $imageName = time().'.'.request()->image->getClientOriginalExtension();

        $image_resize = Image::make($image->getRealPath());  

        $image_resize->resize(200, 100);
        //dd($image_resize);
        $image_resize->save(public_path('images/').$imageName);

        $room  = new Room;
        $room ->title = $request->title;
        $room->image = $imageName;
        $room ->save();  
        return back()->with('message', "Комната успешно добавлено!!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Room::findOrFail($id);

        return view('admin.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Room::findOrFail($id);

        return view('admin.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'title' => 'required|unique:rooms|string|max:200',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048'
        ]);

        $image = request()->image;
        $imageName = time().'.'.request()->image->getClientOriginalExtension();

        $image_resize = Image::make($image->getRealPath());  

        $image_resize->resize(200, 50);

        $image_resize->save(public_path('images/').$imageName);


        $room  = Room::find($id);
        $room ->title = $request->title;
        $room->image = $imageName;
        $room ->save();  
        return back()->withInput()->with('message', "Комната успешно изменена!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room  = Room::findOrFail($id);
        $room ->delete();
        return redirect()->route('room.index')->with('message', 'Комната успешно удалена!!');
    }
}
