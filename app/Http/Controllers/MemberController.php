<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Room;
use App\BookingProcess;
use App\Services\Helper;
use App\Partner;
use Carbon\Carbon;

class MemberController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$row = Room::all();
        $res = Helper::changeStatus($row);
        $data = Room::all();

        return view('member.home', compact('data'));
    }


    /**
     * @param  $id
     * @return \Illuminate\Http\Response
     */

    public function bookingRooms($id)
    {
        $data = BookingProcess::where('user_id', $id)->get();
        
        return view('member.member_room', compact('data'));
    }




    /**
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$data = BookingProcess::where('room_id', $id)->get();

        return view('member.show', compact('data'));
    }



    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {

        if(strtotime($request->start) > strtotime($request->finish) or strtotime($request->start) < strtotime(Carbon::now()))
            return back()->with('success', 'Неправильный формат даты, пожалуйста выберите правильную дату!!');
        
        $response = Helper::isValid($request->start, $request->finish, $request->rooms_id);
        if(!$response){
            return back()->with('success', 'В это время комната занято, пожалуйста выберите другую дату!!');
        }

        
    	$data = new BookingProcess;
    	$data->user_id = Auth::user()->id;
    	$data->room_id = $request->rooms_id;
    	$data->user_name = Auth::user()->name;
    	$data->room_title = $request->room_title;
    	$data->status = 1;
    	$data->booking_start = $request->start;
    	$data->booking_finish = $request->finish;

    	$data->save();
        $booking_processes_id = BookingProcess::latest('id')->first('id');
        Helper::setPartners($request->name, $booking_processes_id->id);

        return back()->with('success', 'Комната успешно забронировано!!');

        //return response()->json(['message'=>'Got Simple Product Ajax Request.']);
    }


    /**
     * @param int  $id
     * @return \Illuminate\Http\Response
     */

    public function getOptions($id)
    {
        $d = BookingProcess::find($id);
        if(empty($d))
            $partners = [];
        $partners = BookingProcess::find($id)->partners;
        $room_id = BookingProcess::whereId(34)->get('room_id');
        //dd($room_id);
        return view('member.options', compact('partners', 'room_id'));
    }






    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function extension(Request $request)
    {
        $validFinishTime = BookingProcess::whereId($request->id)->get('booking_finish')->toArray();
        $id = $request->id;
        if(!(strtotime($request->start) < strtotime($request->finish)))
            return back()->with('message', 'Неправильный формат даты, пожалуйста выберите правильную дату!!');
        if(strtotime($request->finish) < strtotime($validFinishTime[0]['booking_finish']))
            return back()->with('message', 'Неправильный формат даты, пожалуйста выберите правильную дату!!');

        $response = Helper::isValid($request->start, $request->finish, $request->room_id, $id);
        if(!$response)
            return back()->with('message', 'В это время комната занято, пожалуйста выберите другую дату!!');

        $row = [
            'user_name' => Auth::user()->name,
            'room_title' => $request->room_title,
            'status' => 1,
            'booking_start' => $request->start,
            'booking_finish' => $request->finish
        ];
        BookingProcess::whereId($id)->update($row);

        return back()->with('message', 'Комната успешно продлено!!');
    }





    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function bookingDestroy($id)
    {
        $data = BookingProcess::findOrFail($id);
        $data->status = 0;
        $data->save();

        return back()->with('message', 'Успешное завершение!!');
    }



}
