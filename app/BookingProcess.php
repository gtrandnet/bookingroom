<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingProcess extends Model
{

	protected $fillable = [
     'user_id', 'room_id', 'user_name','room_title','booking_start','booking_finish'
    ];

	
    public function partners(){
    	return $this->hasMany(Partner::class);
    }

    public function rooms(){
    	return $this->hasMany(Room::class);
    }

    public function setChangeStatus(){
        $this->attributes['status'] = 0;
    }


}
