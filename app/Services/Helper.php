<?php

namespace App\Services;
 
use App\Partner;
use App\BookingProcess;
use Carbon\Carbon;


class Helper
{
    /**
     * @param  int $start, $finish, $rooms_id
     * @return response for validation date booking 
     */


    public static function isValid($start, $finish, $rooms_id, $id=Null)
    {
        if(isset($id) && $id != Null)
        {
            $rooms = BookingProcess::where('room_id', $rooms_id)->where('id', '!=', $id)->where('status', '!=', 0)->get()->toArray();
        }else{
            $rooms = BookingProcess::where('room_id', $rooms_id)->where('status', '!=', 0)->get()->toArray();
        }

		$res = true;
    	if ($rooms){
    	foreach ($rooms as $room) 
        	{
        		if (!(strtotime($start) < strtotime($room['booking_start']) && strtotime($finish) < strtotime($room['booking_start']) or strtotime($start) >= strtotime($room['booking_finish']) && strtotime($finish) >= strtotime($room['booking_finish'])) ) 
        		{
        				$res = false;
                        //dd($res);
        		}
        	}
        }

        return $res;
    }



    /**
     * @param  $data, $booking_processes_id
     * @return response for set Partners into partners DB table 
     */


    public static function setPartners($data, $booking_processes_id)
    {
    	global $first;

    	if(count($data)==0)
    		return;
    	
    	$first = $data[0];
    	if(empty($first))
    		return;
    	$partner = new Partner;
    	$partner->name = $first;
    	$partner->booking_process_id = $booking_processes_id;
    	$partner->save();

    	array_splice($data, 0, 1);

    	Helper::setPartners($data, $booking_processes_id);
    	return true;
    }



    public static function changeStatus($data)
    {
        global $count;

        if(count($data)==0)
            return;
        $count = $data[0]->bookingprocess;
        if(empty($count))
            return;
        foreach ($count as $key => $value) {
            if(strtotime(Carbon::now()) >= strtotime($value->booking_finish))
            {
                $value->status = 0;
                $value->save(); 
                continue;
            } 
        }
        
        $data->splice(0, 1);
        Helper::changeStatus($data);

        return true;
    }
    //if(strtotime($start) > strtotime(Carbon::today()) && strtotime($start) < strtotime(Carbon::tomorrrow()))
    public static function isToday($start)
    {
        if(strtotime($start) > strtotime(Carbon::today()) && strtotime($start) < strtotime(Carbon::now()->addDays(2)))
            return true;
        return false;
    }


    public static function isNow($start, $finish)
    {
        if (strtotime($start) <= strtotime(Carbon::now()) && strtotime($finish) > strtotime(Carbon::now()) )
            return true;
        return false;
    }

    
}
